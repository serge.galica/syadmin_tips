#!/bin/bash

###############################################################
#
#  AUTEUR:   Xavier
#
#  DESCRIPTION: list top history command
###############################################################

# commande premier élément
echo "Sur premier champs"
echo "##################"
cat ~/.bash_history | awk '{a[$1]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head

# commande deux élément
echo ""
echo "Sur deux éléments"
echo "#################"
cat ~/.bash_history | awk '{a[$1" "$2]++}END{for(i in a){print a[i] " " i}}' | sort -rn | head

# commande plus simple mais moins bonne
echo ""
echo "avec uniq -c"
echo "############"
cat ~/.bash_history | sort |uniq -c | sort -rn | head
